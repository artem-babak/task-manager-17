package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    void clear();

}
