package ru.t1c.babak.tm.api.model;

public interface IHaveId {

    String getId();

    void setId(String id);

}