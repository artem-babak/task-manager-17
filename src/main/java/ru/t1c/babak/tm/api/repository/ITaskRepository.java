package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

    void clear();

}
