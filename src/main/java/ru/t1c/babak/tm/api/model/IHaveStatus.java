package ru.t1c.babak.tm.api.model;

import ru.t1c.babak.tm.enumerated.Status;

public interface IHaveStatus {

    Status getStatus();

    void setStatus(Status status);

}