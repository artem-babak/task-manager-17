package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.util.TerminalUtil;

public final class TaskRemoveFromProjectByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "TaskAddToProjectByIdCommand";

    public static final String DESCRIPTION = "Remove relation from task to project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("\tENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("\tENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getServiceLocator().getProjectTaskService().unbindTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
    }

}
