package ru.t1c.babak.tm.command.system;

import ru.t1c.babak.tm.Application;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class VersionCommand extends AbstractSystemCommand {

    public static final String NAME = "version";

    public static final String ARGUMENT = "-v";

    public static final String DESCRIPTION = "Show application version.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    /**
     * Getting version from pom.xml ${project.version}
     */
    @Override
    public void execute() {
        System.out.println("[VERSION]");
        final Properties properties = new Properties();
        final InputStream inputStream = Application.class
                .getClassLoader()
                .getResourceAsStream("project.properties");
        try {
            properties.load(inputStream);
            System.out.println(properties.getProperty("version"));
            if (inputStream != null) inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
