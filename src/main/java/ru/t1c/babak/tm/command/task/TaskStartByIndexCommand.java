package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-index";

    public static final String DESCRIPTION = "Start task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

}
