package ru.t1c.babak.tm.command.project;

import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";

    public static final String DESCRIPTION = "Create new project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        System.out.println("\tENTER BEGIN DATE");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("\tENTER END DATE");
        final Date dateEnd = TerminalUtil.nextDate();
        getProjectService().create(name, description, dateBegin, dateEnd);
    }

}
